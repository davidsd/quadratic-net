{-# LANGUAGE DataKinds           #-}
{-# LANGUAGE LambdaCase          #-}
{-# LANGUAGE NoFieldSelectors    #-}
{-# LANGUAGE OverloadedRecordDot #-}
{-# LANGUAGE OverloadedStrings   #-}
{-# LANGUAGE TypeOperators       #-}

module QuadraticNet.SDP where

import Bootstrap.Math.Linear     (toM)
import Bootstrap.Math.Polynomial qualified as Pol
import Control.Monad.IO.Class    (MonadIO)
import Control.Monad.Trans       (lift)
import Control.Monad.Trans.Maybe (MaybeT (..), runMaybeT)
import Data.Binary               (Binary)
import Data.Distributive         (distribute)
import Data.Foldable             qualified as Foldable
import Data.List.NonEmpty        qualified as NE
import Data.Matrix               qualified as MU
import Data.Matrix.Static        (Matrix)
import Data.Matrix.Static        qualified as M
import Data.MultiSet             qualified as MultiSet
import Data.Text                 qualified as T
import Data.Vector               qualified as V
import Fmt                       ((+||), (||+))
import GHC.TypeNats              (KnownNat, type (-))
import Linear.Metric             (dot)
import Numeric.Eigen.Raw         (eigenvaluesSymmetric, eigenvectorsSymmetric)
import Numeric.Eigen.Static      (fromRawMatrix)
import Numeric.Rounded           (toDouble)
import QuadraticNet.Util         (logInfo)
import SDPB                      (BigFloat, Params (..))
import SDPB qualified
import SDPB.Simple               qualified as SDPB
import Type.Reflection           (Typeable)

defaultSDPBConfig :: FilePath -> FilePath -> FilePath -> SDPB.SDPBConfig
defaultSDPBConfig pmp2sdpExecutable sdpbExecutable workDir = SDPB.SDPBConfig
  { SDPB.sdpbParams = SDPB.defaultParams
    { initialMatrixScalePrimal = 1e8
    , initialMatrixScaleDual   = 1e8
    , dualityGapThreshold      = 1e-40
    , maxComplementarity       = 1e40
    , precision                = 384
    }
  , SDPB.pmp2sdpExecutable = pmp2sdpExecutable
  , SDPB.sdpbExecutable = sdpbExecutable
  , SDPB.workDir = workDir
  }

data SDPRelaxationConfig = SDPRelaxationConfig
  { maxRankMinimizationSteps     :: Int
  , eigenvalueReductionThreshold :: Double
  , eigenvalueSizeThreshold      :: Double
  , sdpbConfig                   :: SDPB.SDPBConfig
  } deriving (Show)

defaultSDPRelaxationConfig :: FilePath -> FilePath -> FilePath -> SDPRelaxationConfig
defaultSDPRelaxationConfig pmp2sdpExec sdpbExec workDir = SDPRelaxationConfig
  { maxRankMinimizationSteps     = 20
  , eigenvalueReductionThreshold = 0.7
  , eigenvalueSizeThreshold      = 1e-32
  , sdpbConfig                   = defaultSDPBConfig pmp2sdpExec sdpbExec workDir
  }

unitMatrixSymmetric :: (KnownNat j, Num a) => Int -> Int -> Matrix j j a
unitMatrixSymmetric i j = M.matrix $ \e ->
  if e == (i,j) || e == (j,i)
  then 1
  else 0

enumerated :: [a] -> [(T.Text, a)]
enumerated xs = [(T.pack (show @Int i), x) | (i,x) <- zip [0 ..] xs]

generateSymIndices :: Int -> (Int -> Int -> a) -> V.Vector a
generateSymIndices dim f = V.fromList $ do
  i <- [1 .. dim]
  j <- [1 .. i]
  return (f i j)

data PureConstSDP f a = MkPureConstSDP
  { objective     :: V.Vector a
  , normalization :: V.Vector a
  , constraints   :: f (MU.Matrix (V.Vector a))
  }

pureConstSDPToSDP :: (Binary a, Typeable a, Floating a, Eq a, Foldable f) => PureConstSDP f a -> SDPB.SDP m a
pureConstSDPToSDP pureSdp = SDPB.SDP
  { SDPB.objective = pureObjective "objective" pureSdp.objective
  , SDPB.normalization = pureNormalization "normalization" pureSdp.normalization
  , SDPB.constraints = do
      c <- Foldable.toList pureSdp.constraints
      pure $ pureConstantConstraint "constraint" c
  }

pureObjective :: (Binary a, Typeable a) => T.Text -> V.Vector a -> SDPB.Objective m a
pureObjective name v =
  SDPB.MkObjective
  (pure (SDPB.MkObjectiveChunk (SDPB.Pure v) label))
  label
  where
    label = SDPB.mkLabel name v

pureNormalization :: (Binary a, Typeable a) => T.Text -> V.Vector a -> SDPB.Normalization m a
pureNormalization name v =
  SDPB.MkNormalization
  (pure (SDPB.MkNormalizationChunk (SDPB.Pure v) label))
  label
  where
    label = SDPB.mkLabel name v

pureConstraint
  :: (Binary a, Typeable a, Floating a, Eq a)
  => T.Text
  -> MU.Matrix (V.Vector (Pol.Polynomial a))
  -> SDPB.PositiveConstraint m a
pureConstraint name mat =
  SDPB.MkPositiveConstraint
  (pure (SDPB.MkPositiveConstraintChunk (SDPB.Pure (V.singleton positiveMatrix)) label))
  label
  where
    positiveMatrix = SDPB.MkPositiveMatrix mat MultiSet.empty (exp (-1)) Nothing
    label = SDPB.mkLabel name mat

pureConstantConstraint
  :: (Binary a, Typeable a, Floating a, Eq a)
  => T.Text
  -> MU.Matrix (V.Vector a)
  -> SDPB.PositiveConstraint m a
pureConstantConstraint name mat = pureConstraint name (fmap (fmap Pol.constant) mat)

vecOfMatToMatOfVec :: KnownNat j => V.Vector (Matrix j j a) -> MU.Matrix (V.Vector a)
vecOfMatToMatOfVec ms = M.unpackStatic (distribute ms)

data WithPSD a = MkWithPSD
  { psdConstraint :: a
  , others        :: [a]
  } deriving (Functor, Foldable)

addConstraint :: a -> WithPSD a -> WithPSD a
addConstraint c (MkWithPSD p q) = MkWithPSD p (c:q)

sdpRelaxation
  :: forall j a . (KnownNat j, Eq a, Floating a, Binary a, Typeable a)
  => NE.NonEmpty (Matrix j j a)
  -> PureConstSDP WithPSD a
sdpRelaxation qs = MkPureConstSDP
  { objective     = zeroVec
  , normalization = traceVec
  , constraints   = MkWithPSD psdConstraint [vecOfMatToMatOfVec (qConstraint q) | q <- NE.toList qs]
  }
  where
    dim = M.nrows (NE.head qs)
    zeroVec  = generateSymIndices dim (\_ _ -> 0)
    traceVec = generateSymIndices dim (\i j -> if i == j then 1 else 0)
    psdConstraint = vecOfMatToMatOfVec @j $
      generateSymIndices dim unitMatrixSymmetric
    qConstraint q = generateSymIndices dim $ \i j -> toM @1 @1 $
      (q M.! (i,j)) * (if i == j then 1 else 2)

rankMinimization
  :: (KnownNat j, KnownNat (j-1), KnownNat p)
  => PureConstSDP WithPSD (BigFloat p)
  -> Matrix j j (BigFloat p)
  -> PureConstSDP WithPSD (BigFloat p)
rankMinimization sdp xPsd = MkPureConstSDP
  { objective     = V.cons (-1) sdp.objective
  , normalization = V.cons 0 sdp.normalization
  , constraints = addConstraint rankMinimizer $ fmap (fmap (V.cons 0)) sdp.constraints
  }
  where
    smallestEigV = smallestEigenvectors xPsd
    dim = M.nrows smallestEigV
    rankMinimizer = vecOfMatToMatOfVec $
      M.identity `V.cons`
      generateSymIndices dim (\i j -> -M.transpose smallestEigV M..* unitMatrixSymmetric i j M..* smallestEigV)

smallestEigenvectors
  :: KnownNat p
  => Matrix j j (BigFloat p)
  -> Matrix j (j-1) (BigFloat p)
smallestEigenvectors = M.applyUnary $
  \m -> MU.submatrix 1 (MU.nrows m) 1 (MU.ncols m - 1) (eigenvectorsSymmetric m)

solveRelaxation
  :: (KnownNat j, KnownNat p)
  => SDPB.SDPBConfig
  -> PureConstSDP WithPSD (BigFloat p)
  -> IO (Maybe (Matrix j j (BigFloat p)))
solveRelaxation sdpbConfig sdp =
  SDPB.withSDPSolution sdpbConfig (pureConstSDPToSDP sdp) $ \(input, output) ->
  if SDPB.isDualFeasible output
  then fmap Just $ do
    let m = fromRawMatrix sdp.constraints.psdConstraint
    alpha <- SDPB.readFunctional (SDPB.outDir input)
    return $ fmap (alpha `dot`) m
  else return Nothing

reportEigenvalues :: (KnownNat p, MonadIO m) => Matrix j j (BigFloat p) -> m ()
reportEigenvalues m =
  logInfo $ "SDP solution eigenvalues: "+||fmap toDouble (eigenvaluesSymmetric (M.unpackStatic m))||+""

minimizeSDPRank
  :: (KnownNat j, KnownNat (j-1), KnownNat p)
  => SDPRelaxationConfig
  -> PureConstSDP WithPSD (BigFloat p)
  -> Matrix j j (BigFloat p)
  -> IO (Matrix j j (BigFloat p))
minimizeSDPRank cfg sdp mPsd =
  go cfg.maxRankMinimizationSteps mPsd (maxAbsSmallEigenvalues mPsd)
  where
    reductionThresh = realToFrac cfg.eigenvalueReductionThreshold
    sizeThresh = realToFrac cfg.eigenvalueSizeThreshold
    go 0 m _ = return m
    go n m smallEvals =
      if smallEvals < sizeThresh
      then return m
      else do
        let sdp' = rankMinimization sdp m
        solveRelaxation cfg.sdpbConfig sdp' >>= \case
          Nothing -> do
            logInfo "Couldn't solve rank minimization. This shouldn't happen."
            return m
          Just m' -> do
            reportEigenvalues m'
            let smallEvals' = maxAbsSmallEigenvalues m'
            if smallEvals' < reductionThresh * smallEvals
              then go (n-1) m' smallEvals'
              else do
                logInfo "Rank minimization did not sufficiently reduce eigenvalues. Stopping."
                return (if smallEvals' < smallEvals then m' else m)

maxAbsSmallEigenvalues :: KnownNat p => Matrix j j (BigFloat p) -> BigFloat p
maxAbsSmallEigenvalues = V.maximum . fmap abs . V.init . eigenvaluesSymmetric . M.unpackStatic

solveSDPRelaxationSmallRank
  :: (KnownNat j, KnownNat (j-1), KnownNat p)
  => SDPRelaxationConfig
  -> NE.NonEmpty (Matrix j j (BigFloat p))
  -> IO (Maybe (Matrix j j (BigFloat p)))
solveSDPRelaxationSmallRank cfg quadraticForms =
  let sdp = sdpRelaxation quadraticForms
  in runMaybeT $ do
    m <- MaybeT $ solveRelaxation (cfg.sdpbConfig { SDPB.sdpbParams = cfg.sdpbConfig.sdpbParams
                                                    { SDPB.findDualFeasible = True
                                                    , SDPB.detectDualFeasibleJump = True
                                                    }}) sdp
    lift $ reportEigenvalues m
    lift $ minimizeSDPRank cfg sdp m
