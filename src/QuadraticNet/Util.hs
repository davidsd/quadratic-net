{-# LANGUAGE OverloadedStrings #-}

module QuadraticNet.Util where

import Control.Monad.IO.Class (MonadIO, liftIO)
import Data.Text              (Text)
import Data.Text.IO           qualified as T
import System.IO              (hFlush, stderr)

logInfo :: MonadIO m => Text -> m ()
logInfo msg = liftIO $ T.hPutStrLn stderr ("[QN] " <> msg) >> hFlush stderr
