{-# LANGUAGE DataKinds           #-}
{-# LANGUAGE NoFieldSelectors    #-}
{-# LANGUAGE OverloadedRecordDot #-}

module QuadraticNet.Newton where

import Data.List.NonEmpty   qualified as NE
import Data.Matrix          as MU
import Data.Matrix.Static   qualified as M
import Data.Vector          qualified as V
import GHC.TypeNats         (KnownNat)
import Linear.Metric        as L
import Linear.V             (V)
import Linear.V             qualified as L
import Linear.Vector        as L
import Numeric.Eigen.Raw    (luSolve)
import Numeric.Eigen.Static (fromRawVector)
import Numeric.Rounded      (Precision, Rounded, Rounding)
import QuadraticNet.Math    (bilinearPair, matXvec, normalizeV, outerProduct,
                             sumM)

data NewtonConfig = NewtonConfig
  { maxNewtonSteps        :: Int
  , newtonSearchThreshold :: Double
  } deriving (Show)

defaultNewtonConfig :: NewtonConfig
defaultNewtonConfig = NewtonConfig
  { maxNewtonSteps        = 1000
  , newtonSearchThreshold = 1e-16
  }

data NewtonResult a
  = MaxNewtonSteps a
  | Converged Int a
  deriving (Show, Functor)

-- | The hessian and gradient of -log|x^T Q x / x^T x|. TODO:
-- implement a more general potential.
hessianAndGradientTerm
  :: (KnownNat j, Fractional a)
  => V j a
  -> M.Matrix j j a
  -> (M.Matrix j j a, V j a)
hessianAndGradientTerm x q = (hessian, gradient)
  where
    qx = q `matXvec` x
    xqx = x `dot` qx
    xx = x `dot` x
    hessian =
      (-2/xqx) *^ q
      + (4/(xqx*xqx)) *^ outerProduct qx qx
      + (2/xx) *^ M.identity
      - (4/(xx*xx)) *^ outerProduct x x
    gradient = ((2/xx) *^ x) - ((2/xqx) *^ qx)

-- | Compute the Newton search direction for the potential V(x), subject
-- to the constraint x^T x = 1, where
--
-- V(x) = \sum_Q -log|x^T Q x/ x^T x|
--
newtonDirection
  :: (KnownNat j, Rounding r, Precision p)
  => NE.NonEmpty (M.Matrix j j (Rounded r p))
  -> V j (Rounded r p)
  -> V j (Rounded r p)
newtonDirection qForms x =
  fromRawVector $ V.init $
  luSolve constrainedHessian ((-1) *^ constrainedGradient)
  where
    (hessians, gradients) = NE.unzip (fmap (hessianAndGradientTerm x) qForms)
    hessian = M.unpackStatic $ sumM hessians
    gradient = L.toVector $ L.sumV gradients
    constrainedHessian = MU.joinBlocks
      ( hessian,          2 *^ MU.colVector (L.toVector x)
      , 2 *^ MU.rowVector (L.toVector x), MU.zero 1 1
      )
    constrainedGradient = V.snoc gradient 0

newtonSearch
  :: (KnownNat j, Rounding r, Precision p)
  => NewtonConfig
  -> NE.NonEmpty (M.Matrix j j (Rounded r p))
  -> V j (Rounded r p)
  -> NewtonResult (V j (Rounded r p))
newtonSearch cfg qForms xInitial =
  fmap normalizeV (go cfg.maxNewtonSteps xInitial)
  where
    go 0 x = MaxNewtonSteps x
    go n x =
      let dx = newtonDirection qForms x
      in if L.norm dx < realToFrac cfg.newtonSearchThreshold
         then Converged (cfg.maxNewtonSteps - n) x
         else go (n-1) (takeFeasibleStep qForms x dx)

takeFeasibleStep :: (KnownNat j, Fractional a, Ord a) => NE.NonEmpty (M.Matrix j j a) -> V j a -> V j a -> V j a
takeFeasibleStep qForms x s =
  let x' = x + s
  in if all (\q -> bilinearPair q x' > 0) qForms
     then x'
     else takeFeasibleStep qForms x (s ^/ 2)

