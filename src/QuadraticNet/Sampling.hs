{-# LANGUAGE OverloadedStrings #-}

module QuadraticNet.Sampling where

import Data.List.NonEmpty    qualified as NE
import Data.Matrix.Static    qualified as M
import Data.Maybe            (fromMaybe)
import Fmt                   ((+||), (||+))
import GHC.TypeNats          (KnownNat)
import Linear.V              (V)
import Numeric.Eigen.Static  (choleskyDecomposition)
import Numeric.LinearAlgebra qualified as LA
import Numeric.Rounded       (Precision, Rounded, Rounding)
import QuadraticNet.Math     (bilinearPair, fromHVector, matXvec, normalizeV)
import QuadraticNet.Util     (logInfo)
import System.Random         (randomIO)

samplePoints
  :: forall j r p . (KnownNat j, Rounding r, Precision p)
  => NE.NonEmpty (M.Matrix j j (Rounded r p))
  -> M.Matrix j j (Rounded r p)
  -> Int
  -> IO (Maybe (V j (Rounded r p)))
samplePoints qForms covariance numSamples = do
  logInfo $ "Sampling up to "+||numSamples||+" points with covariance "+||M.toLists covariance||+""
  go numSamples
  where
    l = fromMaybe (error "Covariance matrix is not positive semidefinite")
      (choleskyDecomposition covariance)
    size = M.nrows covariance
    gaussianRandomVector = do
      seed <- randomIO
      pure $
        fmap realToFrac $
        fromHVector $
        LA.randomVector seed LA.Gaussian size
    randomVectorWithCovariance = normalizeV . matXvec l <$> gaussianRandomVector
    isFeasible x = all (\q -> bilinearPair q x > 0) qForms

    go 0 = pure Nothing
    go n = do
      x <- randomVectorWithCovariance
      case isFeasible x of
        False -> go (n-1)
        True -> do
          logInfo $ "Sample point "+||numSamples - n + 1||+"/"+||numSamples||+" is feasible."
          pure $ Just x
