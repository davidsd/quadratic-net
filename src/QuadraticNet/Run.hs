{-# LANGUAGE DataKinds           #-}
{-# LANGUAGE LambdaCase          #-}
{-# LANGUAGE NoFieldSelectors    #-}
{-# LANGUAGE OverloadedRecordDot #-}
{-# LANGUAGE OverloadedStrings   #-}
{-# LANGUAGE TypeOperators       #-}

module QuadraticNet.Run where

import Control.Monad.Catch       (SomeException, try)
import Control.Monad.Except      (ExceptT (..), withExceptT)
import Control.Monad.IO.Class    (liftIO)
import Control.Monad.Trans.Maybe (MaybeT (..), runMaybeT)
import Data.Foldable             (toList)
import Data.List.NonEmpty        qualified as NE
import Data.Matrix.Static        qualified as M
import Data.Text                 (Text)
import Data.Text                 qualified as T
import Fmt                       ((+||), (||+))
import GHC.TypeNats
import Linear.V                  (V)
import Linear.Vector             ((*^))
import QuadraticNet.Newton       (NewtonConfig (..), NewtonResult (..),
                                  defaultNewtonConfig, newtonSearch)
import QuadraticNet.Sampling     (samplePoints)
import QuadraticNet.SDP
import QuadraticNet.Util         (logInfo)
import SDPB                      (BigFloat)

data QuadraticNetConfig = QuadraticNetConfig
  { sdpRelaxationConfig :: SDPRelaxationConfig
  , newtonConfig        :: NewtonConfig
  , numSamplePoints     :: Int
  , covarianceFuzzing   :: Double
  , sdpBarrierShift     :: Double
  , precision           :: Int
  } deriving (Show)

defaultQuadraticNetConfig :: FilePath -> FilePath -> FilePath -> QuadraticNetConfig
defaultQuadraticNetConfig pvmExec sdpbExec workDir = QuadraticNetConfig
  { sdpRelaxationConfig = defaultSDPRelaxationConfig pvmExec sdpbExec workDir
  , newtonConfig        = defaultNewtonConfig
  , numSamplePoints     = 10000
  , covarianceFuzzing   = 1e-16
  , sdpBarrierShift     = 1e-16
  , precision           = 128
  }

whenNothing :: Monad m => MaybeT m a -> m () -> MaybeT m a
whenNothing m go = MaybeT $ runMaybeT m >>= \case
  Nothing -> go >> return Nothing
  just -> return just

tryExcept :: IO a -> ExceptT Text IO a
tryExcept go = withExceptT (T.pack . show @SomeException) (ExceptT (try go))

runSearch
  :: (KnownNat j, KnownNat (j-1), KnownNat p)
  => QuadraticNetConfig
  -> NE.NonEmpty (M.Matrix j j (BigFloat p))
  -> ExceptT Text IO (Maybe (V j (BigFloat p)))
runSearch cfg qForms = runMaybeT $ do
  let size = M.nrows (NE.head qForms)
      qFormsShifted = fmap (\q -> q - realToFrac cfg.sdpBarrierShift *^ M.identity) qForms
  logInfo $ "Config: "+||cfg||+""
  logInfo $ "Quadratic net with "+||length qForms||+" quadratic form(s) of size "+||size||+"x"+||size||+"."
  logInfo $ "Quadratic forms: "+||map M.toLists (NE.toList qForms)||+""
  covariance <- MaybeT (tryExcept $ solveSDPRelaxationSmallRank cfg.sdpRelaxationConfig qFormsShifted)
    `whenNothing` (logInfo "No solution to sdp relaxation.")
  let c = (realToFrac cfg.covarianceFuzzing)^(2 :: Int)
      covariance' =
        (1 - c) *^ covariance + c *^ M.identity
  xFeasible <- MaybeT (tryExcept $ samplePoints qForms covariance' cfg.numSamplePoints)
    `whenNothing` (logInfo "No feasible sample points found.")
  liftIO $ case newtonSearch cfg.newtonConfig qForms xFeasible of
    MaxNewtonSteps x -> do
      logInfo "Warning: max newton steps exceeded"
      logInfo $ "Feasible point: "+||toList x||+""
      return x
    Converged steps x -> do
      logInfo $ "Newton search converged after "+||steps||+" steps."
      logInfo $ "Feasible point: "+||toList x||+""
      return x
